// TestConsole.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include "..\cplate\Generator.h"

int _tmain(int argc, _TCHAR* argv[])
{
    cplate::Generator gen;

    gen.RootDictionary()->SetValue(L"CLASS_NAME", L"MyClass");
    gen.RootDictionary()->SetValue(L"RESULT", L"void");

    cplate::Dictionary* func1(gen.RootDictionary()->CreateChild(L"FUNC1"));
    func1->SetValue(L"NAME", L"TestFunction1");
    func1->SetValue(L"PARAMS", L"int i, int b");

    cplate::Dictionary* func2(gen.RootDictionary()->CreateChild(L"FUNC2"));
    func2->SetValue(L"NAME", L"TestFunction2");
    func2->SetValue(L"PARAMS", L"float x, float y, float z");

    func2 = gen.RootDictionary()->CreateChild(L"FUNC2");
    func2->SetValue(L"NAME", L"OtherTestFunction2");
    func2->SetValue(L"PARAMS", L"float _x, float _y, float _z");

    gen.Execute(L"test.tpl", L"output.cpp");

    system("notepad.exe output.cpp");

    return 0;
}
