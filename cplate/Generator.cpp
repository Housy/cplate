//
// Copyright 2013 Huysentruit Wouter
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "Generator.h"
#include <fstream>
#include <regex>

using namespace std;
using namespace cplate;

Generator::Generator()
    : m_rootDictionary(new Dictionary())
{
}

Generator::~Generator()
{
}

namespace
{
    wstring ReadCompleteFile(const wstring& fileName)
    {
        wifstream stream(fileName);
        return wstring(istreambuf_iterator<wchar_t>(stream), istreambuf_iterator<wchar_t>());
    }

    wstring Normalize(const wstring& source)
    {
        // if inner template open/close tags are only surrounded by whitespace, we will remove whitespace and eol
        const wregex expr(L"^[ \\t]*(/\\*((<[a-zA-Z0-9_]+)|([a-zA-Z0-9_]+>))\\*/)[ \\t]*\\n");
        const wstring replace = L"$1";
        wstring dest = regex_replace(source, expr, replace);
        return dest;
    }

    void GeneratePart(const wstring& part, wostream& output, Dictionary* const dictionary)
    {
        auto begin = part.begin();
        // enumerate values
        const wregex expr(L"/\\*=([a-zA-Z0-9_]+)\\*/");
        for (wsregex_iterator i(part.begin(), part.end(), expr); i != wsregex_iterator(); ++i)
        {
            const wsmatch match = *i;
            // generate part before or between value(s)
            const wstring out(match.prefix().first, match.prefix().second);
            output.write(out.c_str(), out.length());
            // generate value
            const wstring name(match[1].first, match[1].second);
            wstring value;
            if (!dictionary->GetValue(name, value))
            {   // if the value doesn't exist in the dictionary, output the tag as is
                const wstring tag(match[0].first, match[0].second);
                output.write(tag.c_str(), tag.length());
            }
            else
                output.write(value.c_str(), value.length());
            begin = match.suffix().first;
        }
        // generate part after last value (or complete part if no values)
        const wstring out(begin, part.end());
        output.write(out.c_str(), out.length());
    }

    bool Generate(const wstring& templ, wostream& output, Dictionary* const dictionary)
    {
        auto begin = templ.begin();
        // enumerate inner templates
        const wregex expr(L"/\\*<([a-zA-Z0-9_]+)\\*/([^]+)(/\\*\\1>\\*/)");
        for (wsregex_iterator i(templ.begin(), templ.end(), expr); i != wsregex_iterator(); ++i)
        {
            const wsmatch match = *i;
            // generate part of template before or between inner template(s)
            const wstring part(match.prefix().first, match.prefix().second);
            GeneratePart(part, output, dictionary);
            // innerTemplate
            const wstring tag(match[1].first, match[1].second);
            const wstring innerTempl(match[2].first, match[2].second);
            auto innerDictionaries = dictionary->GetChildrenOrSelf(tag);
            for (auto innerDictionary = innerDictionaries.begin(); innerDictionary != innerDictionaries.end(); ++innerDictionary)
                if (!Generate(innerTempl, output, *innerDictionary))
                    return false;
            begin = match.suffix().first;
        }
        // generate part of template after last inner template (or complete template if no inner templates)
        const wstring part(begin, templ.end());
        GeneratePart(part, output, dictionary);
        return true;
    }
}

bool Generator::Execute(const wstring& templateFileName, const wstring& outputFileName) const
{
    wstring templ = ReadCompleteFile(templateFileName);
    wofstream outputStream(outputFileName);
    templ = Normalize(templ);
    return Generate(templ, outputStream, m_rootDictionary.get());
}

Dictionary* const Generator::RootDictionary() const
{
    return m_rootDictionary.get();
}
