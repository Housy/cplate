//
// Copyright 2013 Huysentruit Wouter
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#include "Dictionary.h"

using namespace std;
using namespace cplate;

Dictionary::Dictionary(const wstring& tag, Dictionary* const parent)
    : m_tag(tag)
    , m_parent(parent)
{
}

Dictionary::~Dictionary()
{
}

bool Dictionary::SetValue(const wstring& name, const wstring& value)
{
    m_values[name] = value;
    return true;
}

bool Dictionary::RemoveValue(const wstring& name)
{
    auto i = m_values.find(name);
    if (i == m_values.end())
        return false;
    m_values.erase(i);
    return true;
}

bool Dictionary::GetValue(const std::wstring& name, std::wstring& value) const
{
    // if this dictionary contains the value, return it
    auto i = m_values.find(name);
    if (i != m_values.end())
    {
        value = m_values.at(name);
        return true;
    }
    // this dictionary doesn't contain the value, ask parent
    if (m_parent != nullptr)
        return m_parent->GetValue(name, value);
    // no more parents (we're at the root dictionary) and value not found
    return false;
}

Dictionary* const Dictionary::CreateChild(const std::wstring& tag)
{
    shared_ptr<Dictionary> child(new Dictionary(tag, this));
    m_children.insert(m_children.end(), pair<std::wstring, shared_ptr<Dictionary>>(tag, child));
    return child.get();
}

list<Dictionary*> Dictionary::GetChildrenOrSelf(const std::wstring& tag) const
{
    list<Dictionary*> result;
    for (auto i = m_children.begin(); i != m_children.end(); ++i)
    {
        if (i->second->m_tag == tag)
            result.insert(result.end(), i->second.get());
    }
    if (result.empty())
        result.insert(result.end(), const_cast<Dictionary*>(this));
    return result;
}
