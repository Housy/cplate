#pragma once

#include <memory>
#include <string>
#include "Dictionary.h"

namespace cplate
{
    class Generator
    {
    public:
        Generator();
        virtual ~Generator();

        bool Execute(const std::wstring& templateFileName, const std::wstring& outputFileName) const;
        Dictionary* const RootDictionary() const;

    protected:
        std::shared_ptr<Dictionary> m_rootDictionary;
    };
}
