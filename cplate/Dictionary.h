#pragma once

#include <memory>
#include <string>
#include <map>
#include <list>

namespace cplate
{
    class Generator;
    class Dictionary
    {
        friend class Generator;

    public:
        virtual ~Dictionary();

        bool SetValue(const std::wstring& name, const std::wstring& value);
        bool RemoveValue(const std::wstring& name);
        bool GetValue(const std::wstring& name, std::wstring& value) const;

        Dictionary* const CreateChild(const std::wstring& tag);
        std::list<Dictionary*> GetChildrenOrSelf(const std::wstring& tag) const;

    protected:
        Dictionary(const std::wstring& tag = L"", Dictionary* const parent = nullptr);

        const std::wstring m_tag;
        Dictionary* const m_parent;

        std::map<std::wstring, std::wstring> m_values;
        std::multimap<std::wstring, std::shared_ptr<Dictionary>> m_children;
    };
}
